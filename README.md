# ColorPicker

**本项目是基于开源项目ColorPicker进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/duanhong169/ColorPicker ）追踪到原项目版本**

#### 项目介绍

- 项目名称：ColorPicker
- 所属系列：ohos的第三方组件适配移植
- 功能： 颜色选择器
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/duanhong169/ColorPicker
- 原项目基线版本：1.1.6 ，sha1:7be88d07371865aa1075a633ef19b4077cda43d4
- 编程语言：Java
- 外部库依赖：无

#### 效果
<img src="screenshot/operation.gif"/>

#### 安装教程

方法一：

1. 编译har包ColorPicker.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“app->libs”下。

3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。
```groovy
repositories {
    flatDir { dirs 'libs' }
}
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
    implementation(name: 'ColorPicker', ext: 'har')
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法二：

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址
```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```
2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:
```
dependencies {
    implementation 'top.defaults.ohos:colorpicker:1.0.0'
}
```

#### 使用说明

1.布局里添加ColorPickerView：
  ```xml
     <top.defaults.colorpicker.ColorPickerView
         ohos:id="$+id:colorPickerView"
         ohos:height="match_content"
         ohos:width="match_parent"
         ohos:align_parent_left="true"
         ohos:align_parent_right="true"
         ohos:align_parent_top="true"
         app:enableAlpha="true"/>
  ```
2.可初始指定颜色:
```java
   colorPickerView.setInitialColor(INITIAL_COLOR);
```
3.可重置颜色:
```java
colorPickerView.reset();
```
4.获取选择的颜色：
```java
     colorPickerView.subscribe((color, fromUser, shouldPropagate) -> {
            // color 选中的颜色

        });
```
#### 版本迭代


- v1.0.0


#### 版权和许可信息

    Copyright 2018 Hong Duan
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
        http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

