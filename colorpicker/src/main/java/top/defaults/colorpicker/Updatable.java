package top.defaults.colorpicker;


import ohos.multimodalinput.event.TouchEvent;

public interface Updatable {

    void update(TouchEvent event);
}
