package top.defaults.colorpicker;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

import static top.defaults.colorpicker.Constants.SELECTOR_RADIUS_DP;

public class ColorWheelSelector extends Component implements Component.DrawTask {

    private Paint selectorPaint;
    private float selectorRadiusPx = SELECTOR_RADIUS_DP * 3;
    private Point currentPoint = new Point();

    public ColorWheelSelector(Context context) {
        super(context);
        init();
    }

    public ColorWheelSelector(Context context, AttrSet attrs) {
        super(context,attrs);
        init();
    }

    public ColorWheelSelector(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        selectorPaint = new Paint();
        selectorPaint.setAntiAlias(true);
        selectorPaint.setColor(Color.BLACK);
        selectorPaint.setStyle(Paint.Style.STROKE_STYLE);
        selectorPaint.setStrokeWidth(2);
        addDrawTask(this::onDraw);
    }

    @Override
    public void invalidate() {
        super.invalidate();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawLine(currentPoint.getPointX() - selectorRadiusPx, currentPoint.getPointY(),
                currentPoint.getPointX() + selectorRadiusPx, currentPoint.getPointY(), selectorPaint);
        canvas.drawLine(currentPoint.getPointX(), currentPoint.getPointY() - selectorRadiusPx,
                currentPoint.getPointX(), currentPoint.getPointY() + selectorRadiusPx, selectorPaint);
        canvas.drawCircle(currentPoint.getPointX(), currentPoint.getPointY(), selectorRadiusPx * 0.66f, selectorPaint);
    }

    public void setSelectorRadiusPx(float selectorRadiusPx) {
        this.selectorRadiusPx = selectorRadiusPx;
    }

    public void setCurrentPoint(Point currentPoint) {
        this.currentPoint = currentPoint;
        invalidate();
    }
}
