package top.defaults.colorpicker;


import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.PopupDialog;
import ohos.agp.window.service.WindowManager;
import ohos.app.Context;

import java.util.Locale;


public class ColorPickerPopup {

    private Context context;
    private PopupDialog popupWindow;
    private int initialColor;
    private boolean enableBrightness;
    private boolean enableAlpha;
    private String okTitle;
    private String cancelTitle;
    private boolean showIndicator;
    private boolean showValue;
    private boolean onlyUpdateOnTouchEventUp;
    private ShapeElement shapeElement;

    private ColorPickerPopup(Builder builder) {
        this.context = builder.context;
        this.initialColor = builder.initialColor;
        this.enableBrightness = builder.enableBrightness;
        this.enableAlpha = builder.enableAlpha;
        this.okTitle = builder.okTitle;
        this.cancelTitle = builder.cancelTitle;
        this.showIndicator = builder.showIndicator;
        this.showValue = builder.showValue;
        this.onlyUpdateOnTouchEventUp = builder.onlyUpdateOnTouchEventUp;
    }

    public void show(final ColorPickerObserver observer) {
        show(null, observer);
    }

    public void show(Component parent, final ColorPickerObserver observer) {
        LayoutScatter inflater = LayoutScatter.getInstance(context);
        if (inflater == null) return;
        Component layout = inflater.parse(ResourceTable.Layout_top_defaults_view_color_picker_popup, null, false);
        final ColorPickerView colorPickerView = (ColorPickerView) layout.findComponentById(ResourceTable.Id_colorPickerView);
        popupWindow = new PopupDialog(context, null);
        popupWindow.setBackColor(Color.WHITE);
        popupWindow.setAutoClosable(true);
        popupWindow.setSize(1050, 1200);
        colorPickerView.setInitialColor(initialColor);
        colorPickerView.setEnabledBrightness(enableBrightness);
        colorPickerView.setEnabledAlpha(enableAlpha);
        colorPickerView.setOnlyUpdateOnTouchEventUp(onlyUpdateOnTouchEventUp);
        colorPickerView.subscribe(observer);
        Text cancel = (Text) layout.findComponentById(ResourceTable.Id_cancle);
        cancel.setText(cancelTitle);
        cancel.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                popupWindow.hide();
            }
        });
        Text ok = (Text) layout.findComponentById(ResourceTable.Id_ok);
        ok.setText(okTitle);
        ok.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                popupWindow.hide();
                if (observer != null) {
                    observer.onColorPicked(colorPickerView.getColor());
                }
            }
        });

        final Text colorIndicator = (Text) layout.findComponentById(ResourceTable.Id_selectIndicator);
        final Text colorHex = (Text) layout.findComponentById(ResourceTable.Id_selectHex);

        colorIndicator.setVisibility(showIndicator ? Component.VISIBLE : Component.INVISIBLE);
        colorHex.setVisibility(showValue ? Component.VISIBLE : Component.INVISIBLE);

        if (showIndicator) {
            if (shapeElement == null) {
                shapeElement = new ShapeElement();
            }
            shapeElement.setShape(ShapeElement.RECTANGLE);
            shapeElement.setRgbColor(RgbColor.fromArgbInt(initialColor));
            colorIndicator.setBackground(shapeElement);
        }
        if (showValue) {
            colorHex.setText(colorHex(initialColor));
        }
        colorPickerView.subscribe(new ColorObserver() {
            @Override
            public void onColor(int color, boolean fromUser, boolean shouldPropagate) {
                if (showIndicator) {
                    shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
                    colorIndicator.setBackground(shapeElement);
                }
                if (showValue) {
                    colorHex.setText(colorHex(color));
                }
            }
        });

        popupWindow.setAlignment(LayoutAlignment.CENTER);
        popupWindow.setCustomComponent(layout);

        popupWindow.show();
    }

    public void dismiss() {
        if (popupWindow != null) {
            popupWindow.hide();
        }
    }

    public static class Builder {

        private Context context;
        private int initialColor = Color.MAGENTA.getValue();
        private boolean enableBrightness = true;
        private boolean enableAlpha = false;
        private String okTitle = "OK";
        private String cancelTitle = "Cancel";
        private boolean showIndicator = true;
        private boolean showValue = true;
        private boolean onlyUpdateOnTouchEventUp = false;

        public Builder(Context context) {
            this.context = context;
        }

        public Builder initialColor(int color) {
            initialColor = color;
            return this;
        }

        public Builder enableBrightness(boolean enable) {
            enableBrightness = enable;
            return this;
        }


        public Builder enableAlpha(boolean enable) {
            enableAlpha = enable;
            return this;
        }

        public Builder okTitle(String title) {
            okTitle = title;
            return this;
        }

        public Builder cancelTitle(String title) {
            cancelTitle = title;
            return this;
        }

        public Builder showIndicator(boolean show) {
            showIndicator = show;
            return this;
        }

        public Builder showValue(boolean show) {
            showValue = show;
            return this;
        }

        public Builder onlyUpdateOnTouchEventUp(boolean only) {
            onlyUpdateOnTouchEventUp = only;
            return this;
        }

        public ColorPickerPopup build() {
            return new ColorPickerPopup(this);
        }
    }

    private String colorHex(int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        int a = Color.alpha(color);
        int r = rgbColor.getRed();
        int g = rgbColor.getGreen();
        int b = rgbColor.getBlue();
        return String.format(Locale.getDefault(), "0x%02X%02X%02X%02X", a, r, g, b);
    }

    public abstract static class ColorPickerObserver implements ColorObserver {
        public abstract void onColorPicked(int color);

        @Override
        public final void onColor(int color, boolean fromUser, boolean shouldPropagate) {

        }
    }
}
