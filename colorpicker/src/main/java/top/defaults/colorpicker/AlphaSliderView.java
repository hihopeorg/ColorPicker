package top.defaults.colorpicker;


import ohos.agp.colors.HsvColor;
import ohos.agp.components.AttrSet;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;
import ohos.media.image.PixelMap;


public class AlphaSliderView extends ColorSliderView {

    private PixelMap backgroundBitmap;
    private Canvas backgroundCanvas;

    public AlphaSliderView(Context context) {
        super(context);
    }

    public AlphaSliderView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public AlphaSliderView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected float resolveValue(int color) {
        return Color.alpha(color) / 255.f;
    }

    protected void configurePaint(Paint colorPaint) {
        HsvColor hsv = HsvColor.toHSV(baseColor);
        if (hsv.getHue() == 0.0f) {
            hsv.setHue(300.f);
        }
        int startColor = HsvColor.toColor(0, hsv.getHue(), hsv.getSaturation(), hsv.getValue());
        int endColor = HsvColor.toColor(255, hsv.getHue(), hsv.getSaturation(), hsv.getValue());

        Shader shader = new LinearShader(new Point[]{new Point(0, 0), new Point(getWidth(), getHeight())}, new float[]{0, 0, getWidth(), getHeight()},
                new Color[]{new Color(startColor), new Color(endColor)}, Shader.TileMode.CLAMP_TILEMODE);
        colorPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);

    }

    protected int assembleColor() {
        HsvColor hsv = HsvColor.toHSV(baseColor);
        int alpha = (int) (currentValue * 255);
        return HsvColor.toColor(alpha, hsv.getHue(), hsv.getSaturation(), hsv.getValue());
    }
}
