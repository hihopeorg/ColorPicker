package top.defaults.colorpicker;


import ohos.agp.colors.HsvColor;
import ohos.agp.components.AttrSet;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

public class BrightnessSliderView extends ColorSliderView {
    private int initCount = 1;

    public BrightnessSliderView(Context context) {
        super(context);
    }

    public BrightnessSliderView(Context context, AttrSet attrs) {
        super(context, attrs);
    }

    public BrightnessSliderView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected float resolveValue(int color) {
        return HsvColor.toHSV(color).getValue() / 100f;
    }

    protected void configurePaint(Paint colorPaint) {
        HsvColor hsv = HsvColor.toHSV(baseColor);

        if (hsv.getHue() == 0.0f) {
            hsv.setHue(300.f);
        }
        hsv.setValue(0);
        int startColor = HsvColor.toColor(255, hsv.getHue(), hsv.getSaturation(), hsv.getValue());
        hsv.setValue(100);
        int endColor = HsvColor.toColor(255, hsv.getHue(), hsv.getSaturation(), hsv.getValue());
        Shader shader = new LinearShader(new Point[]{new Point(0, 0), new Point(getWidth(), getHeight())}, new float[]{0, 0, getWidth(), getHeight()},
                new Color[]{new Color(startColor), new Color(endColor)}, Shader.TileMode.CLAMP_TILEMODE);
        colorPaint.setShader(shader, Paint.ShaderType.LINEAR_SHADER);
    }

    protected int assembleColor() {
        HsvColor hsv = HsvColor.toHSV(baseColor);
        hsv.setValue(currentValue * 100);
        return HsvColor.toColor(255, hsv.getHue(), hsv.getSaturation(), hsv.getValue());
    }
}
