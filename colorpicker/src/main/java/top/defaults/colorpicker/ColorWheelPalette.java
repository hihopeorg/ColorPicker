package top.defaults.colorpicker;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.*;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

public class ColorWheelPalette extends ComponentContainer implements ComponentContainer.ArrangeListener, Component.DrawTask {

    /**
     * Colors to construct the color wheel using
     */
    private static final Color[] COLORS = new Color[]{Color.RED, Color.MAGENTA, Color.BLUE, Color.CYAN, Color.GREEN, Color.YELLOW, Color.RED};

    private float radius;
    private float centerX;
    private float centerY;

    private Paint huePaint;
    private Paint saturationPaint;

    public ColorWheelPalette(Context context) {
        super(context);
        init();
    }

    public ColorWheelPalette(Context context, AttrSet attrs) {
        super(context, attrs);
        init();
    }

    public ColorWheelPalette(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        huePaint = new Paint();
        huePaint.setAntiAlias(true);
        saturationPaint = new Paint();
        saturationPaint.setAntiAlias(true);
        setArrangeListener(this::onArrange);
        addDrawTask(this::onDraw);
    }

    @Override
    public boolean onArrange(int i, int i1, int w, int h) {
        int netWidth = w - getPaddingLeft() - getPaddingRight();
        int netHeight = h - getPaddingTop() - getPaddingBottom();
        radius = Math.min(netWidth, netHeight) * 0.5f;
        if (radius < 0) return false;
        centerX = w * 0.5f;
        centerY = w * 0.5f;
        Shader hueShader = new SweepShader(centerX, centerY, COLORS, null);
        huePaint.setShader(hueShader, Paint.ShaderType.SWEEP_SHADER);

        Shader saturationShader = new RadialShader(new Point(centerX, centerY), radius, new float[]{0, 0, centerX, centerY},
                new Color[]{Color.WHITE, new Color(0x00FFFFFF)}, Shader.TileMode.CLAMP_TILEMODE);
        saturationPaint.setShader(saturationShader, Paint.ShaderType.RADIAL_SHADER);
        return false;
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.drawCircle(centerX, centerY, radius, huePaint);
        canvas.drawCircle(centerX, centerY, radius, saturationPaint);
    }
}
