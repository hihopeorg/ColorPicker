package top.defaults.colorpicker;


import ohos.agp.colors.HsvColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.DisplayAttributes;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import static top.defaults.colorpicker.Constants.SELECTOR_RADIUS_DP;

/**
 * HSV color wheel
 */
public class ColorWheelView extends StackLayout implements ColorObservable, Updatable, Component.EstimateSizeListener,
        ComponentContainer.ArrangeListener, Component.TouchEventListener {

    private float radius;
    private float centerX;
    private float centerY;

    private float selectorRadiusPx = SELECTOR_RADIUS_DP * 3;

    private Point currentPoint = new Point();
    private int currentColor = Color.MAGENTA.getValue();
    private boolean onlyUpdateOnTouchEventUp;

    private ColorWheelSelector selector;

    private ColorObservableEmitter emitter = new ColorObservableEmitter();
    private ThrottledTouchEventHandler handler = new ThrottledTouchEventHandler(this);

    private static final int MODE_SHIFT = 30;
    private static final int MODE_MASK = 0x3 << MODE_SHIFT;
    private Context mContext;

    public ColorWheelView(Context context) {
        super(context);
        mContext = context;
        initPalette();
        init();
    }

    public ColorWheelView(Context context, AttrSet attrs) {
        super(context, attrs);
        mContext = context;
        initPalette();
        init();
    }

    public ColorWheelView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initPalette();
        init();
    }

    private void init() {
        setEstimateSizeListener(this::onEstimateSize);
        setTouchEventListener(this::onTouchEvent);
        setArrangeListener(this::onArrange);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int heightSize = EstimateSpec.getSize(heightMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int widthSize = EstimateSpec.getSize(widthMeasureSpec);
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);
        int width, height;
        width = height = Math.min(widthSize, heightSize);
        width = makeMeasureSpec(width, EstimateSpec.PRECISE);
        height = makeMeasureSpec(height, EstimateSpec.PRECISE);
        return false;
    }

    private void initPalette() {
        DisplayAttributes displayAttributes = DisplayManager.getInstance().getDefaultDisplay(mContext).get().getAttributes();
        selectorRadiusPx = SELECTOR_RADIUS_DP * displayAttributes.scalDensity;
        {
            LayoutConfig layoutParams = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT);
            ColorWheelPalette palette = new ColorWheelPalette(mContext);
            int padding = (int) selectorRadiusPx;
            palette.setPadding(padding, padding, padding, padding);
            addComponent(palette, layoutParams);
        }

        {
            LayoutConfig selectParams = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                    ComponentContainer.LayoutConfig.MATCH_CONTENT);
            selector = new ColorWheelSelector(mContext);
            selector.setSelectorRadiusPx(selectorRadiusPx);
            addComponent(selector, selectParams);
        }
    }

    @Override
    public boolean onArrange(int i, int i1, int w, int h) {
        int netWidth = 1036;
        int netHeight = 1036;
        radius = Math.min(netWidth, netHeight) * 0.5f - selectorRadiusPx;
        if (radius < 0) return false;
        centerX = netWidth * 0.5f;
        centerY = netWidth * 0.5f;
        setColor(currentColor, false);
        return false;
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        int action = event.getAction();
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
            case TouchEvent.POINT_MOVE:
                handler.onTouchEvent(event);
                return true;
            case TouchEvent.PRIMARY_POINT_UP:
                update(event);
                return true;
        }
        return false;
    }


    private int getColorAtPoint(float eventX, float eventY) {
        float x = eventX - centerX;
        float y = eventY - centerY;
        double r = Math.sqrt(x * x + y * y);

        HsvColor hsvColor = new HsvColor(0, 0, 100);
        hsvColor.setHue((float) (Math.atan2(y, -x) / Math.PI * 180f) + 180);
        hsvColor.setSaturation(Math.max(0f, Math.min(1f, (float) (r / radius))) * 100);
        return HsvColor.toColor(255, hsvColor.getHue(), hsvColor.getSaturation(), hsvColor.getValue());
    }

    public void setOnlyUpdateOnTouchEventUp(boolean onlyUpdateOnTouchEventUp) {
        this.onlyUpdateOnTouchEventUp = onlyUpdateOnTouchEventUp;
    }

    public void setColor(int color, boolean shouldPropagate) {
        HsvColor hsv = HsvColor.toHSV(color);
        float r = (hsv.getSaturation() / 100f) * radius;
        if (color == Color.MAGENTA.getValue()) {
            hsv.setHue(300);
        }
        float radian = (float) (hsv.getHue() / 180f * Math.PI);
        updateSelector((float) (r * Math.cos(radian) + centerX), (float) (-r * Math.sin(radian) + centerY));
        currentColor = color;
        if (!onlyUpdateOnTouchEventUp) {
            emitter.onColor(color, false, shouldPropagate);
        }
    }

    private void updateSelector(float eventX, float eventY) {
        float x = eventX - centerX;
        float y = eventY - centerY;
        double r = Math.sqrt(x * x + y * y);
        if (r > radius) {
            x *= radius / r;
            y *= radius / r;
        }
        currentPoint.modify(x + centerX, y + centerY);
        selector.setCurrentPoint(currentPoint);
    }

    @Override
    public void subscribe(ColorObserver observer) {
        emitter.subscribe(observer);
    }

    @Override
    public void unsubscribe(ColorObserver observer) {
        emitter.unsubscribe(observer);
    }

    @Override
    public int getColor() {
        return emitter.getColor();
    }

    @Override
    public void update(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(0);
        float x = point.getX();
        float y = point.getY();
        boolean isTouchUpEvent = event.getAction() == TouchEvent.PRIMARY_POINT_UP;
        if (!onlyUpdateOnTouchEventUp || isTouchUpEvent) {
            emitter.onColor(getColorAtPoint(x, y), true, isTouchUpEvent);
        }
        updateSelector(x, y);
    }

    public int makeMeasureSpec(int size,
                               int mode) {
        return (size & ~MODE_MASK) | (mode & MODE_MASK);
    }

}
