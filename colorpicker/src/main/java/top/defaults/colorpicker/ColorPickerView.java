package top.defaults.colorpicker;


import java.util.ArrayList;
import java.util.List;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.utils.Color;
import ohos.app.Context;


public class ColorPickerView extends DirectionalLayout implements ColorObservable, Component.EstimateSizeListener {

    private ColorWheelView colorWheelView;
    private BrightnessSliderView brightnessSliderView;
    private AlphaSliderView alphaSliderView;
    private ColorObservable observableOnDuty;
    private boolean onlyUpdateOnTouchEventUp;

    private int initialColor = Color.BLACK.getValue();

    private int sliderMargin;
    private int sliderHeight;

    private static final int MODE_SHIFT = 30;
    private static final int MODE_MASK = 0x3 << MODE_SHIFT;

    private boolean enableAlpha;
    private boolean enableBrightness;

    public ColorPickerView(Context context) {
        super(context);
        init(context);
    }

    public ColorPickerView(Context context, AttrSet attrs) {
        super(context, attrs);
        initAttr(attrs);
        init(context);
    }

    public ColorPickerView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttr(attrs);
        init(context);
    }

    public void setOnlyUpdateOnTouchEventUp(boolean onlyUpdateOnTouchEventUp) {
        this.onlyUpdateOnTouchEventUp = onlyUpdateOnTouchEventUp;
        updateObservableOnDuty();
    }

    private void initAttr(AttrSet attrs) {
        enableAlpha = AttrSetUtil.getBoolean(attrs, "enableAlpha", false);
        enableBrightness = AttrSetUtil.getBoolean(attrs, "enableBrightness", true);
        onlyUpdateOnTouchEventUp = AttrSetUtil.getBoolean(attrs, "onlyUpdateOnTouchEventUp", false);
    }

    private void init(Context context) {
        setOrientation(VERTICAL);
        colorWheelView = new ColorWheelView(context);
        int margin = 24;
        sliderMargin = 2 * margin;
        sliderHeight = 48;

        LayoutConfig params = new LayoutConfig();
        params.width = 1080;
        params.height = 1080;
        addComponent(colorWheelView, params);
        setEnabledBrightness(enableBrightness);
        setEnabledAlpha(enableAlpha);

        setPadding(margin, margin, margin, margin);
        addListener();
    }

    private void addListener() {
        setEstimateSizeListener(this::onEstimateSize);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int maxHeight = EstimateSpec.getSize(heightMeasureSpec);
        int heightMode = EstimateSpec.getMode(heightMeasureSpec);
        int maxWidth = EstimateSpec.getSize(widthMeasureSpec);
        int widthMode = EstimateSpec.getMode(widthMeasureSpec);

        int desiredWidth = maxHeight - (getPaddingTop() + getPaddingBottom()) + (getPaddingLeft() + getPaddingRight());
        if (brightnessSliderView != null) {
            desiredWidth -= (sliderMargin + sliderHeight);
        }
        if (alphaSliderView != null) {
            desiredWidth -= (sliderMargin + sliderHeight);
        }


        int width = Math.min(maxWidth, desiredWidth);
        int height = width - (getPaddingLeft() + getPaddingRight()) + (getPaddingTop() + getPaddingBottom());
        if (brightnessSliderView != null) {
            height += (sliderMargin + sliderHeight);
        }
        if (alphaSliderView != null) {
            height += (sliderMargin + sliderHeight);
        }

        return false;
    }


    public int makeMeasureSpec(int size,
                               int mode) {
        return (size & ~MODE_MASK) | (mode & MODE_MASK);
    }


    public void setInitialColor(int color) {
        initialColor = color;
        colorWheelView.setColor(color, true);
    }

    public void setEnabledBrightness(boolean enable) {
        if (enable) {
            if (brightnessSliderView == null) {
                brightnessSliderView = new BrightnessSliderView(getContext());
                LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, sliderHeight);
                params.setMarginTop(sliderMargin);
                addComponent(brightnessSliderView, params);
            }
            brightnessSliderView.bind(colorWheelView);
            updateObservableOnDuty();
        } else {
            if (brightnessSliderView != null) {
                brightnessSliderView.unbind();
                removeComponent(brightnessSliderView);
                brightnessSliderView = null;
            }
            updateObservableOnDuty();
        }

        if (alphaSliderView != null) {
            setEnabledAlpha(true);
        }
    }

    public void setEnabledAlpha(boolean enable) {
        if (enable) {
            if (alphaSliderView == null) {
                alphaSliderView = new AlphaSliderView(getContext());
                LayoutConfig params = new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, sliderHeight);
                params.setMarginTop(sliderMargin);
                addComponent(alphaSliderView, params);
            }

            ColorObservable bindTo = brightnessSliderView;
            if (bindTo == null) {
                bindTo = colorWheelView;
            }
            alphaSliderView.bind(bindTo);
            updateObservableOnDuty();
        } else {
            if (alphaSliderView != null) {
                alphaSliderView.unbind();
                removeComponent(alphaSliderView);
                alphaSliderView = null;
            }
            updateObservableOnDuty();
        }
    }

    private void updateObservableOnDuty() {
        if (observableOnDuty != null && observers.size() > 0) {
            for (ColorObserver observer : observers) {
                observableOnDuty.unsubscribe(observer);
            }
        }

        colorWheelView.setOnlyUpdateOnTouchEventUp(false);
        if (brightnessSliderView != null) {
            brightnessSliderView.setOnlyUpdateOnTouchEventUp(false);
        }
        if (alphaSliderView != null) {
            alphaSliderView.setOnlyUpdateOnTouchEventUp(false);
        }

        if (brightnessSliderView == null && alphaSliderView == null) {
            observableOnDuty = colorWheelView;
            colorWheelView.setOnlyUpdateOnTouchEventUp(onlyUpdateOnTouchEventUp);
        } else {
            if (alphaSliderView != null) {
                observableOnDuty = alphaSliderView;
                alphaSliderView.setOnlyUpdateOnTouchEventUp(onlyUpdateOnTouchEventUp);
            } else {
                observableOnDuty = brightnessSliderView;
                brightnessSliderView.setOnlyUpdateOnTouchEventUp(onlyUpdateOnTouchEventUp);
            }
        }

        if (observers != null && observers.size() > 0) {
            for (ColorObserver observer : observers) {
                observableOnDuty.subscribe(observer);
                observer.onColor(observableOnDuty.getColor(), false, true);
            }
        }
    }

    public void reset() {
        colorWheelView.setColor(initialColor, true);
    }

    List<ColorObserver> observers = new ArrayList<>();

    @Override
    public void subscribe(ColorObserver observer) {
        if (observableOnDuty != null) {
            observableOnDuty.subscribe(observer);
        }

        observers.add(observer);
    }

    @Override
    public void unsubscribe(ColorObserver observer) {
        if (observableOnDuty != null) {
            observableOnDuty.unsubscribe(observer);
        }

        observers.remove(observer);
    }

    @Override
    public int getColor() {
        return observableOnDuty.getColor();
    }


}
