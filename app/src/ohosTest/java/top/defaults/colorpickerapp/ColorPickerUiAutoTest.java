/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package top.defaults.colorpickerapp;


import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import top.defaults.colorpicker.*;

import java.util.Locale;


/**
 * Ui功能自动化
 */
public class ColorPickerUiAutoTest extends TestCase {
    private Context context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
    private MainAbility ability;
    private ShapeElement element;

    @Before
    public void setUp() throws Exception {
        ability = EventHelper.startAbility(MainAbility.class);
        Thread.sleep(5000);
    }

    @After
    public void tearDown() throws Exception {
        EventHelper.clearAbilities();
    }

    @Test
    public void testSelector() {
        ColorPickerView colorPickerView = (ColorPickerView) ability.findComponentById(ResourceTable.Id_colorPickerView);
        ColorWheelView colorWheelView = (ColorWheelView) colorPickerView.getComponentAt(0);
        assertNotNull(colorWheelView);

        EventHelper.inputSwipe(ability, 700, 400, 200, 400, 3000);
        assertEquals(colorWheelView.getColor(), colorPickerView.getColor());
    }

    @Test
    public void testAlpha() {
        ColorPickerView colorPickerView = (ColorPickerView) ability.findComponentById(ResourceTable.Id_colorPickerView);
        AlphaSliderView alphaSliderView = (AlphaSliderView) colorPickerView.getComponentAt(2);
        assertNotNull(alphaSliderView);
        EventHelper.inputSwipe(ability, 700, 1550, 200, 1550, 3000);
        assertEquals(alphaSliderView.getColor(), colorPickerView.getColor());
    }

    @Test
    public void testBright() {
        ColorPickerView colorPickerView = (ColorPickerView) ability.findComponentById(ResourceTable.Id_colorPickerView);
        BrightnessSliderView brightnessSliderView = (BrightnessSliderView) colorPickerView.getComponentAt(1);
        assertNotNull(brightnessSliderView);
        EventHelper.inputSwipe(ability, 700, 1450, 200, 1450, 3000);
        assertEquals(brightnessSliderView.getColor(), colorPickerView.getColor());
    }

    @Test
    public void testReset() {
        ColorPickerView colorPickerView = (ColorPickerView) ability.findComponentById(ResourceTable.Id_colorPickerView);
        Text text = (Text) ability.findComponentById(ResourceTable.Id_text);

        // 点击reset
        EventHelper.triggerClickEvent(ability, text);

        //颜色面板（ColorWheelView）上的取值与初始值相同
        assertEquals(colorHex(colorPickerView.getColor()), "0xFFFF8000");
    }
    private String colorHex(int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        int a = Color.alpha(color);
        int r = rgbColor.getRed();
        int g = rgbColor.getGreen();
        int b = rgbColor.getBlue();
        return String.format(Locale.getDefault(), "0x%02X%02X%02X%02X", a, r, g, b);
    }
}