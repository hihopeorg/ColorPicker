package top.defaults.colorpickerapp;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;
import top.defaults.colorpicker.ColorPickerPopup;
import top.defaults.colorpicker.ColorPickerView;

import java.util.Locale;

public class MainAbility extends Ability {
    private static final int INITIAL_COLOR = 0xFFFF8000;
    private ColorPickerView colorPickerView;
    private Text reset;
    private Text selectShow;
    private Text selectHex;
    private ShapeElement shapeElement;
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_main);
        colorPickerView = (ColorPickerView) findComponentById(ResourceTable.Id_colorPickerView);
        reset = (Text) findComponentById(ResourceTable.Id_text);
        selectShow = (Text) findComponentById(ResourceTable.Id_selectShow);
        selectHex = (Text) findComponentById(ResourceTable.Id_selectHex);
        colorPickerView.subscribe((color, fromUser, shouldPropagate) -> {
            handleShape(color);
            selectHex.setText(colorHex(color));

        });
        colorPickerView.setInitialColor(INITIAL_COLOR);

        reset.setClickedListener(component -> {
            colorPickerView.reset();
        });
        selectHex.setClickedListener(component -> {
            showPop();
        });
    }
    private void handleShape(int color){
        if (shapeElement ==null) {
            shapeElement = new ShapeElement();
        }
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        selectShow.setBackground(shapeElement);
    }

    private void handleStatuBar(int color) {
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);;
        getWindow().setStatusBarColor(color);
        getWindow().setNavigationBarColor(color);
    }
    private String colorHex(int color) {
        RgbColor rgbColor = RgbColor.fromArgbInt(color);
        int a = Color.alpha(color);
        int r = rgbColor.getRed();
        int g = rgbColor.getGreen();
        int b = rgbColor.getBlue();
        return String.format(Locale.getDefault(), "0x%02X%02X%02X%02X", a, r, g, b);
    }

    private void showPop() {
        new ColorPickerPopup.Builder(this)
                .initialColor(colorPickerView.getColor())
                .enableAlpha(true)
                .okTitle("Choose")
                .cancelTitle("Cancel")
                .showIndicator(true)
                .showValue(true)
                .onlyUpdateOnTouchEventUp(true)
                .build()
                .show(new ColorPickerPopup.ColorPickerObserver() {
                    @Override
                    public void onColorPicked(int color) {
                        colorPickerView.setInitialColor(color);
                    }
                });
    }
}
